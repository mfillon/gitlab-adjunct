export default class Permission {
    host: string;

    constructor(host: string) {
        this.host = host;
    }

    static getPermissionFromPattern(pattern: string): Permission {
        return new Permission(pattern.replace("*://", "").replace("/*", ""));
    }

    get pattern(): string {
        return "*://" + this.host + "/*";
    }

    get scriptId(): string {
        return "script-" + this.host;
    }
}
