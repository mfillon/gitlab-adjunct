import Permission from "../lib/Permission";

describe("Permission constructor work", () => {
    const permission = new Permission("gitlab.com");

    test("Pattern getter works", () => {
        expect(permission.pattern).toBe("*://gitlab.com/*");
    });

    test("ScriptId getter works", () => {
        expect(permission.scriptId).toBe("script-gitlab.com");
    });
});

describe("Permission factory works", () => {
    const permission = Permission.getPermissionFromPattern("*://gitlab.com/*");

    test("Pattern getter works", () => {
        expect(permission.pattern).toBe("*://gitlab.com/*");
    });

    test("ScriptId getter works", () => {
        expect(permission.scriptId).toBe("script-gitlab.com");
    });
});