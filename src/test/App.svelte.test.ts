import "@testing-library/jest-dom";
import { render, screen } from '@testing-library/svelte';
import App from "../App.svelte";

describe("App", () => {
    test("shows the addon title when rendered", () => {
        render(App);
        expect(screen.getByText("GitLab adjunct")).toBeInTheDocument();
        expect(screen.getByText("GitLab adjunct")).toBeVisible();
    });
});
