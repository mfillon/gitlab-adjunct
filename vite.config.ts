import { defineConfig } from 'vitest/config';
import { svelte } from '@sveltejs/vite-plugin-svelte';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [svelte()],
    test: {
        globals: true,
        environment: "jsdom",
        setupFiles: ["test-setup.ts"]
    },
    build: {
        target: "modules",
        sourcemap: "inline",
        rollupOptions: {
            input: { index: "./index.html", content: "./src/content.ts", background: "./src/background.ts" },
            output: {
                entryFileNames: '[name].js'
            }
        },
    }
});
